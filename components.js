angular.module('components', [])

.directive('headerReference', function() {
    return {
        restrict: 'E',
        templateUrl: './templates/header_reference.html'
    };
})

.directive('middleReference', function() {
    var getParameterByName = function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
    var template = getParameterByName('template');
    return {
        restrict: 'E',
        templateUrl: './templates/' + template + '.html'
    };
})

.directive('footerReference', function() {
    return {
        restrict: 'E',
        templateUrl: './templates/footer_reference.html'
    };
});