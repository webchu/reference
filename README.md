# Instalação
- Instale o Node.js (https://nodejs.org/en/download/)
- Para certificar-se que a instalação foi correta, abra um prompt e digite:
```
node -v
```
Deverá ser exibido a versão instalada. Ex.:
```
v4.2.2
```

- Instale o Git para Windows (https://git-for-windows.github.io/)
- Abra o Git Bash
- Crie um diretório para o seu repositório. Ex.:
```
cd /c/repo/
mkdir reference
cd reference
```

- Baixe os arquivos do repositórios
```
git clone https://webchu@bitbucket.org/webchu/reference.git
```

- Instale os módulos/dependências necessárias para stack
```
npm install
```

- Rode a stack
```
gulp
```

- Instale a extensão LiveReload para o Chrome
https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?utm_source=chrome-app-launcher-info-dialog

- Acesse o arquivo hello_world.html